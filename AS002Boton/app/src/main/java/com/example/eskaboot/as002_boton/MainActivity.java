package com.example.eskaboot.as002_boton;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
/*Sumar dos valores que al dar clic al boton muestre el resultado correspondiente*/

    private EditText num1,num2;
    private TextView resultado;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        num1=(EditText)findViewById(R.id.et1);
        num2=(EditText)findViewById(R.id.et2);
        resultado=(TextView)findViewById(R.id.resultado);
    }

    //Este metodo se ejecutara cuando se presione el boton.
    public void sumar(View view){
        String valor1=num1.getText().toString();
        String valor2=num2.getText().toString();
        int num1=Integer.parseInt(valor1);
        int num2=Integer.parseInt(valor2);
        int suma=num1+num2;
        String res=String.valueOf(suma);
        resultado.setText(res);

    }//End method sumar
}
